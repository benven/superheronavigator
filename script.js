
//Tableau contenant des objets
var heroes = [
    {
        name : "Superman",
        age : 43,
        power : "Lasers, surpuissance.",
        description: "Superman est né sur la planète Krypton. Arrivé sur terre lorsqu'il n'était qu'un bébé, il s'évertue désormais à purger Gotham de sa criminalité.",
        id: 1
    },
    { 
        name : "Batman",
        age : 35,
        power : "Dépourvu de super-pouvoir pour combattre le crime, Batman compte uniquement sur sa condition physique et sur ses gadgets. Son costume, conçu pour terrifier les criminels, est en kevlar ce qui le protège des balles, des armes blanches88 alors que sa cape en nomex le protège du feu",
        description: "Afin de lutter activement contre le crime, Bruce Wayne devient un justicier masqué qui agit principalement la nuit. Il est reconnaissable grâce à son costume constitué d'une cape en forme d'ailes de chauve-souris et d'un masque aux oreilles pointues. S'il ne possède pas de super-pouvoirs, il compense par une grande dextérité dans les arts martiaux et par son équipement technologique ultra-sophistiqué (exemple : la Batmobile).",
        id: 2,
    },
    {
        name : "Hulk",
        age : 56,
        power : "Force surhumaine",
        description: "Le scientifique Bruce Banner domine difficilement son courroux. Derrière sa vie paisible de brillant chercheur en physique, assisté par son ancienne compagne Betty Ross, il dissimule un passé douloureux. C'est alors qu'un accident de laboratoire insolite fait éclater les penchants héroïques de Bruce et se déchaîne les démons cachés en lui. Et, à la face du monde, il se transforme en l'être le plus puissant de la planête.",
        id : 3
    },
    {
        name : "Spiderman",
        age : 34,
        power : "Capacité à sécreter de la toile, agilité surhumaine.",
        description: "Après avoir été piqué par une araignée mutante, le collégien timoré Peter Parker subit une étrange transformation génétique. Il acquiert alors d'étonnantes facultés, comme de pouvoir s'accrocher aux surfaces des murs ou fabriquer des fils adhésifs qui lui permettent de se balancer d'un immeuble à l'autre. Avec ses nouveaux pouvoirs, Peter devient un justicier masqué surnommé Spider-Man. Son premier ennemi sera le Green Goblin, un redoutable criminel qui porte une armure de démon vert. Il s'agit en réalité du financier schizophrène Norman Osborn. Ayant découvert la véritable identité de Spider-Man, Goblin lui tend un piège en kidnappant Mary Jane, la bien-aimée de Peter. Il s'ensuit un affrontement ultime entre les deux ennemis.",
        id : 4
    },
    {
        name : "Joker",
        age : 'Inconnu',
        power : "Rire toxique",
        description: "",
        id : 5
    },
    {
        name : "Deadpool",
        age : 'Inconnu',
        power : "Expert au combat au corps à corps, avec des sabres et armes blanches, guérit rapidement, très bon tireur.",
        description: "Wade Wilson, un ancien militaire des forces spéciales, est devenu mercenaire. Après avoir subi une expérimentation hors-norme qui va accélérer ses pouvoirs de guérison, il va devenir Deadpool. Armé de ses nouvelles capacités et d'un humour noir survolté, il va traquer l'homme qui a bien failli anéantir sa vie.",
        id : 6
    },

    {
        name : "Le Pingouin",
        age : "Le Journaliste a été tué avant d'avoir pu rendre compte de ce champs.",
        power : "Le Journaliste a été tué avant d'avoir pu rendre compte de ce champs.",
        description: "Le Journaliste a été tué avant d'avoir pu rendre compte de ce champs.",
        id : 7
    },



];

//saisie de l'index
let index = prompt("Enter an index or a name to revelate hero : ")
let index_minus = index.toLowerCase();
let find = false;  //Le héro n'est pas trouvé initialement.

if(isNaN(index) || index.length<1){ //Si l'index est de type String on parcours les index pour voir si on match.
    for(i = 0; i < heroes.length; i++){
        let name_hero_minus = heroes[i]['name'].toLowerCase(); 
        if(index_minus == name_hero_minus){
            index = heroes[i]['id'];
            find = true;    //Si match, on passe le boolean a true
        }
    }
}else { //SI un chiffre, on passe a true.
    find = true;
}


if(find){
    //Gestion d'erreur saisie
    if(index < 0 || index > heroes.length){
        index = 1;
        alert("Valeur entre 1 et 6 uniquement. (Ou par string) :p");
    }


    console.log(index);
    index = giveIndex(index);

    let hero_name = heroes[index]['name'];
    createHtml(hero_name)
    createImage(index);
    affiche_description();

} else {
    alert("Malheureusement nous n'avons pas ce superhéro dans notre base de données :(");
    reloadPage();
}

 
/************************ Functions ************************/

function returnIndex(){
    
   //Assigning the variable to the user input
	var chiffre = document.getElementById("input_number").value;

    // to print the input here
    document.getElementById("p2").innerHTML = chiffre;
}

function reloadPage(){
    document.location.reload();
}
function giveIndex(value) {
    
    return value - 1;
}

function createHtml (description) {
    let p = document.createElement('p')
    let node = document.createTextNode(description);
    p.appendChild(node);

    var element = document.getElementById("container");
    var child = document.getElementById("p1");
    element.insertBefore(p, child);
}

function affiche_description(){
   document.getElementById('desc-nom').textContent += heroes[index]['name'];
   document.getElementById('desc-age').textContent += heroes[index]['age'];
   document.getElementById('desc-description').textContent += heroes[index]['description'];
   document.getElementById('desc-pouvoirs').textContent += heroes[index]['power'];

}
//On retourne une image et un fond selon l'index 
function createImage (index) {
    switch(index+1) {
        case 1:
            id = './img/superman.png';
            fond = 'fond_superman';

        break;
        case 2:
            id = './img/batman.png';
            fond = 'fond_batman';
        break;
        case 3:
            id = './img/hulk.png';
            fond = 'fond_hulk';
        break;
        case 4:
            id='./img/spiderman.png';
            fond = 'fond_spiderman';
        break;
        case 5:
            id='./img/joker.png';
            fond = 'fond_joker';
        break;
        case 6:
            id='./img/deadpool.png';
            fond = 'fond_deadpool';
        break;
        case 7:
            id='./img/pingouin.png';
            fond = 'fond_pingouin';
            document.getElementById('myVideo').src = './img/fond_pingouin.mp4';
        break;
    }

    document.getElementById('img').src=id;    //le src de l'image vaut l'id 
    let body = document.querySelector('body'); // ob sélectionne le body
    body.classList.add(fond);                  //... on lui ajoute la classe fond

    
    

    

}
